print('Hello World')

import argparse

parser = argparse.ArgumentParser(description ='Calculate surface and circumference of a circle')
parser.add_argument('radius', type=float, help = 'Radius of circle')
args = parser.parse_args()

def surface(r):
	A = 3.1415926*r**2
	return A

def circumference(r):
	C=2*3.1415*r
	return C

print('Surface =',surface(args.radius))
print('Circumference =', circumference(args.radius))
